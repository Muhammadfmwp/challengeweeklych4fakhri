class App {
  constructor() {
    this.clearButton = document.getElementById("clear-btn");
    this.loadButton = document.getElementById("load-btn");
    this.carContainerElement = document.getElementById("cars-container");
    this.submitButton = document.getElementById("filter-btn")
  }

  async init() {
    await this.load();

    // Register click listener
    this.clearButton.onclick = this.clear;
    this.loadButton.onclick = this.run;
    // this.submitButton.addEventListener("click", () =>{this.filterDriver()} )
    this.submitButton.addEventListener("click", () =>{this.filter()} )
  }

  run = () => {
    Car.list.forEach((car) => {
      const node = document.createElement("div");
      node.className = "grid-item";
      node.innerHTML = car.render();
      this.carContainerElement.appendChild(node);

   
    });

 
  };

  
  filter = () => {

    this.clear();
    const driver = document.getElementById("tipeDriver").value;
    const date = document.getElementById("date").value;
    const penumpang = document.getElementById("jumlahPenumpang").value;
    const waktuJemput = document.getElementById("waktuJemput").value;
    const inputDate = date.slice(8,10);
    const dateInt = parseInt(inputDate);
    const inputWaktu = parseInt(waktuJemput);
      // console.log(inputWaktu);
    Car.list.forEach((car) => {
  
      const supir = String(car.available)
      const tanggalData = car.availableAt.toString();
      const tanggal = parseInt(tanggalData.slice(8,10));
      const jamSedia = parseInt(tanggalData.slice(16,18))
      console.log(dateInt);
      console.log(inputWaktu);
      console.log(driver);


      //If semua filter diisi
      //referensi dari fatwa
      if (inputWaktu <= jamSedia) { 
        if (dateInt >= tanggal) { 
          if (String(driver) == supir) { 
            if (car.capacity >= penumpang) { 
              const node = document.createElement("div");
              node.className = "grid-item";
              node.innerHTML = car.render();
              this.carContainerElement.appendChild(node);
            } 
          } 
        } else if (date == "") { 
          if (String(driver) == supir) { 
            if (car.capacity >= penumpang) { 
              const node = document.createElement("div");
              node.className = "grid-item";
              node.innerHTML = car.render();
              this.carContainerElement.appendChild(node);
            } 
          } 
        } 
      } else if (waktuJemput == "") { 
        if (dateInt >= tanggal) { 
          if (String(driver) == supir) { 
            if (car.capacity >= penumpang) { 
              const node = document.createElement("div");
              node.className = "grid-item";
              node.innerHTML = car.render();
              this.carContainerElement.appendChild(node);
            } 
          } 
        } else if (date == "") { 
          if (String(driver) == supir) { 
            if (car.capacity >= penumpang) { 
              const node = document.createElement("div");
              node.className = "grid-item";
              node.innerHTML = car.render();
              this.carContainerElement.appendChild(node);
            } 
          }
        }
      }
   
    });

 
  };

  async load() {
    const cars = await Binar.listCars();
    Car.init(cars);
  }

  clear = () => {
    let child = this.carContainerElement.firstElementChild;

    while (child) {
      child.remove();
      child = this.carContainerElement.firstElementChild;
    }
  };
}
