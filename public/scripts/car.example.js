class Car {
  static list = [];

  static init(cars) {
    this.list = cars.map((i) => new this(i));
  }

  constructor({
    id,
    plate,
    manufacture,
    model,
    image,
    rentPerDay,
    capacity,
    description,
    transmission,
    available,
    type,
    year,
    options,
    specs,
    availableAt,
  }) {
    this.id = id;
    this.plate = plate;
    this.manufacture = manufacture;
    this.model = model;
    this.image = image;
    this.rentPerDay = rentPerDay;
    this.capacity = capacity;
    this.description = description;
    this.transmission = transmission;
    this.available = available;
    this.type = type;
    this.year = year;
    this.options = options;
    this.specs = specs;
    this.availableAt = availableAt;
  }

  render() {
    return `
    <div class="card2">
     <img src="${this.image}" class="img-card " alt="${this.manufacture}" >
       <p>${this.manufacture}/${this.model}</p>
       <p><b>Rp ${this.rentPerDay} / hari</b></p>
       <p style="max-width:400px">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
       <div class="d-flex">
       <i class="fa fa-thin fa-user-group" style="color: grey"></i>
       <p class="font-weight-light text-small pl-sm-3" 
       style=" 
       line-height: 17px;
       margin-left: 10px">${this.capacity} Orang</p>
       </div>
       <div class="d-flex">
       <i class="fa fa-thin fa-gear" style="color: grey"></i>
       <p class="font-weight-light text-small pl-sm-3"
       style="
       line-height: 17px;
       margin-left: 13px
       "
       >${this.transmission}</p>
       </div>
       <div class="d-flex">
       <i class="fa fa-thin fa-calendar" style="color: grey"></i>
       <p class="font-weight-light text-small pl-sm-3"
       style="
       line-height: 17px;
       margin-left: 15px
       "
       >${this.year}</p>
       </div>
       <button class="btn btn-cars btn-success text-small btn-green pt-2">Pilih Mobil</button>
    </div>
    `;
  }
}
