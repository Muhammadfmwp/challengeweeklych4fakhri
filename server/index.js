const http = require('http');
const { PORT = 8000 } = process.env; 

const fs = require('fs');
const path = require('path');
const PUBLIC_DIRECTORY = path.join(__dirname, '../public'); 

function getHTML(htmlFileName) {
  const htmlFilePath = path.join(PUBLIC_DIRECTORY, htmlFileName);
  return fs.readFileSync(htmlFilePath, 'utf-8')
}


function getBootstrap(req,res){
    if(req.url == '/style.css'){
        res.writeHead(200,{'Content-Type' : 'text/css'});
        const bootsrapFilePath = fs.readFileSync('../public/css/bootstrap.min.css', {encoding:'utf8'});
        res.write(bootsrapFilePath);
        res.end();
    }
}

function onRequest(req, res) {
  switch(req.url) {
    case "/":
      res.writeHead(200)
      getBootstrap(req,res)
      res.end(getHTML("index.html"))
      return;
    case "/about":
      res.writeHead(200)
      res.end(getHTML("about.html"))
      return;
    default:
      res.writeHead(404)
      res.end(getHTML("404.html"))
      return;
  }
}

const server = http.createServer(onRequest);

// Jalankan server
server.listen(PORT, () => {
  console.log("Server sudah berjalan, silahkan buka http://localhost:%d", PORT);
})